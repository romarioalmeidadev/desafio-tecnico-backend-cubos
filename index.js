const express = require("express");
const server = express();
const router = express.Router();
const fs = require("fs");

server.use(express.json({ extended: true }));

const readFile = () => {
  const content = fs.readFileSync("./data/items.json", "utf-8");
  return JSON.parse(content);
};

const writeFile = (content) => {
  const updateFile = JSON.stringify(content);
  fs.writeFileSync("./data/items.json", updateFile, "utf-8");
};

router.get("/", (req, res) => {
  const content = readFile();
  res.send(content);
});

router.post("/", (req, res) => {
  const { dia, intervalo } = req.body;

  const currentContent = readFile();
  const id = Math.ceil(Math.random() * 100);
  currentContent.push({ id, dia, intervalo });
  writeFile(currentContent);
  res.send({ id, dia, intervalo });
});

router.put("/:id", (req, res) => {
  const { idnew } = req.params;

  const { dia, intervalo } = req.body;

  const currentContent = readFile();

  const selecteditem = currentContent.findIndex((item) => item.idnew === idnew);

  const { idnew: cId, dia: cDia, intervalo: cIntervalo } = currentContent[
    selecteditem
  ];

  const newObject = {
    idnew: cId,
    dia: dia ? dia : cDia,
    intervalo: intervalo ? intervalo : cIntervalo,
  };

  currentContent[selecteditem] = newObject;
  writeFile(currentContent);
  res.send(newObject);
});

router.delete("/:id", (req, res) => {
  const { id } = req.params;

  const currentContent = readFile();
  const selecteditem = currentContent.filter((item) => {
    return item.id != parseInt(id);
  });
  currentContent.splice(selecteditem);
  writeFile(selecteditem);
  res.send(selecteditem);
});

server.use(router);

// Caso rota não seja econtrada
server.use((req, res, next) => {
  const erro = new Error("Rota não encontrada");
  erro.status = 404;
  next(erro);
});

server.use((erro, req, res, next) => {
  res.status(erro.status || 500);
  return res.send({
    erro: {
      mensagem: erro.message,
    },
  });
});

server.listen(5000, () => {
  console.log("Servidor Inicializado.");
});
